package com.fin_app.activity.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fin_app.R;
import com.fin_app.activity.model.login.LoginResponseModel;
import com.fin_app.activity.network.RetrofitHelper;
import com.fin_app.activity.utils.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fin_app.activity.utils.Constants.FILL_FORM_TITLE;
import static com.fin_app.activity.utils.Constants.TOKEN;
import static com.fin_app.activity.utils.Constants.TYPE;
import static com.fin_app.activity.utils.Constants.UID;

/**
 * Created by satyendarkumar on 21/10/17.
 */

public class FillFormActivity extends BaseActivity {


    private View mIncludeInvite,mIncludeContact;

    private TextView txtTitle;

    private ImageView imgBack;

    private EditText edtName,edtMobile,edtAddress,edtDate,edtTime;

    private CompositeDisposable mCompositeDisposable;

    private View progressBar;

    private Toolbar toolbar;

    String type,title;
    private int mYear,mMonth,mDay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_form);
        mCompositeDisposable = new CompositeDisposable();
        String type = getIntent().getStringExtra(TYPE);
        String title = getIntent().getStringExtra(FILL_FORM_TITLE);
        initView();
        setOnClickListener();
        if (type != null) {
            if (type.equalsIgnoreCase("invite")) {
                mIncludeInvite.setVisibility(View.VISIBLE);
                mIncludeContact.setVisibility(View.GONE);
                txtTitle.setText(getResources().getString(R.string.str_invite_to_meet));

            } else if (type.equalsIgnoreCase("contact")) {

                mIncludeInvite.setVisibility(View.GONE);
                mIncludeContact.setVisibility(View.VISIBLE);
                txtTitle.setText(getResources().getString(R.string.str_contact_us));

            }else{
                mIncludeInvite.setVisibility(View.VISIBLE);
                mIncludeContact.setVisibility(View.GONE);
                txtTitle.setText(title);
            }
        }
    }

    private void setOnClickListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    private void initView() {
        mIncludeInvite=(View) findViewById(R.id.include_invite_to_meet);
        mIncludeContact=(View) findViewById(R.id.include_contactus);
        txtTitle=(TextView) findViewById(R.id.txtTitle);
        imgBack=(ImageView) findViewById(R.id.imgBack);
        edtName=(EditText) findViewById(R.id.invt_edtname);
        edtMobile=(EditText) findViewById(R.id.invt_edtmobilenumber);
        edtAddress=(EditText) findViewById(R.id.invt_edtaddress);
        edtDate=(EditText) findViewById(R.id.invt_edtdate);
        edtTime=(EditText) findViewById(R.id.invt_edttime);
        progressBar=(View) findViewById(R.id.progressbar);
        toolbar=(Toolbar) findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        edtDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                mYear=mcurrentDate.get(Calendar.YEAR);
                mMonth=mcurrentDate.get(Calendar.MONTH);
                mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(FillFormActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                        edtDate.setText(selectedyear+"/"+selectedmonth+"/"+selectedday);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();  }
        });




        edtTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(FillFormActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        edtTime.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

    }

    public void onClick(View view){

        if(!TextUtils.isEmpty(edtName.getText().toString())
                && !TextUtils.isEmpty(edtMobile.getText().toString())
                && !TextUtils.isEmpty(edtAddress.getText().toString())
                && !TextUtils.isEmpty(edtDate.getText().toString())
                && !TextUtils.isEmpty(edtTime.getText().toString())){

            progressBar.setVisibility(View.VISIBLE);

            mCompositeDisposable.add(RetrofitHelper.getInstance()
                    .getRequestInstance()
                    .inviteToMeet(Prefs.getString(FillFormActivity.this,UID,"")
                            ,Prefs.getString(FillFormActivity.this,TOKEN,"")
                            ,edtName.getText().toString()
                            ,edtMobile.getText().toString()
                             , edtAddress.getText().toString()
                              ,edtDate.getText().toString()
                               ,edtTime.getText().toString())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));



        }else{
            Toast.makeText(this, "Please should not be blank", Toast.LENGTH_SHORT).show();
        }
    }


    private void handleResponse(LoginResponseModel response) {
        progressBar.setVisibility(View.GONE);
        Log.d("constants", "handleResponse: "+response.toString());
        if(response.getSuccess()){

            Intent intent=new Intent(this,MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finishAffinity();
            Toast.makeText(this, "Thank you for your invitation. Our executive will meet you soon.", Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void handleError(Throwable error) {
        progressBar.setVisibility(View.GONE);
        Log.d("constants", "handleerror: "+error.getLocalizedMessage());
        Toast.makeText(this, "Error "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }


    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            updateLabel();
        }

    };

    private void updateLabel(String time) {
        String myFormat = "YY/MM/DD"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edtDate.setText(sdf.format(time));
    }



}
