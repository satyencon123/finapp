package com.fin_app.activity.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fin_app.R;
import com.fin_app.activity.adapter.TrackStatusAdapter;
import com.fin_app.activity.model.TrackStatusModel;

import java.util.ArrayList;

/**
 * Created by satyendarkumar on 22/10/17.
 */

public class TrackStatusActivity extends BaseActivity {

    private TextView txtTitle;
    private ImageView imgBack;
    private RecyclerView mRecyclerView;
    private TrackStatusAdapter trackstatusAdapter;
    private ArrayList<TrackStatusModel> alistModel;
    private Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trackstatus);
        initView();
    }

    private void initView() {
        txtTitle=(TextView) findViewById(R.id.txtTitle);
        imgBack=(ImageView) findViewById(R.id.imgBack);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        mRecyclerView=(RecyclerView) findViewById(R.id.recylerview);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        alistModel=new ArrayList<>();
        for(int i=0;i<20;i++){
            TrackStatusModel model=new TrackStatusModel();
            model.setLoan_type("Home Loan");
            alistModel.add(model);
        }


        trackstatusAdapter=new TrackStatusAdapter(TrackStatusActivity.this,alistModel);
        mRecyclerView.setAdapter(trackstatusAdapter);

        txtTitle.setText(getResources().getString(R.string.str_track_status));

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
