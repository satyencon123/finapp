package com.fin_app.activity.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fin_app.R;
import com.fin_app.activity.model.login.LoginResponseModel;
import com.fin_app.activity.network.RetrofitHelper;
import com.fin_app.activity.utils.Prefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static com.fin_app.activity.utils.Constants.DEVICE_ID;
import static com.fin_app.activity.utils.Constants.LOGIN;
import static com.fin_app.activity.utils.Constants.PHONE_NUMBER;

public class LoginActivity extends BaseActivity {

    private static final int PERMISSION_REQUEST_CODE = 200;

    private EditText edtMobileNumber;
    private TextView txtTitle,txtAppName;
    private Toolbar toolBar;
    private CompositeDisposable mCompositeDisposable;
    private View progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (!checkPermission()) {

            requestPermission();

        } else {

//            Toast.makeText(this, "Already granted", Toast.LENGTH_SHORT).show();

        }
        mCompositeDisposable = new CompositeDisposable();
        initView();
        if(Prefs.getBoolean(this,LOGIN,false)){
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();

        }
    }

    private void initView() {
        edtMobileNumber=(EditText) findViewById(R.id.edtMobileNumber);
        txtTitle=(TextView) findViewById(R.id.txtTitle);
        progressBar=(View) findViewById(R.id.progressbar);
        toolBar=(Toolbar) findViewById(R.id.toolbar);
        txtTitle.setText("What's your mobile number?");

        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onClick(View view){

        if(!TextUtils.isEmpty(edtMobileNumber.getText().toString())){
            progressBar.setVisibility(View.VISIBLE);
            String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            Prefs.saveString(getApplicationContext(),DEVICE_ID,android_id);

            Prefs.saveString(LoginActivity.this,PHONE_NUMBER,edtMobileNumber.getText().toString());

            mCompositeDisposable.add(RetrofitHelper.getInstance()
                    .getRequestInstance()
                    .login(Prefs.getString(LoginActivity.this,DEVICE_ID,"")
                    ,"android",edtMobileNumber.getText().toString())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

        }else {
            Toast.makeText(this, "Please enter the 10 digit mobile number", Toast.LENGTH_SHORT).show();
        }

    }


    private void handleResponse(LoginResponseModel response) {
        progressBar.setVisibility(View.GONE);
        Log.d("constants", "handleResponse: "+response.getMessage());
        if(response.getSuccess()){

            Intent intent=new Intent(this,OtpActivity.class);
            startActivity(intent);

        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void handleError(Throwable error) {
        progressBar.setVisibility(View.GONE);
        Log.d("constants", "handleerror: "+error.getLocalizedMessage());
        Toast.makeText(this, "Error "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
    private void requestPermission() {

//        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, READ_CONTACTS}, PERMISSION_REQUEST_CODE);
        ActivityCompat.requestPermissions(this, new String[]{READ_CONTACTS}, PERMISSION_REQUEST_CODE);

    }


    private boolean checkPermission() {
     //   int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);

//        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
        return  result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted){

                    }
//                        Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    else {

//                        Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }



    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

}
