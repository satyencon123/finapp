package com.fin_app.activity.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fin_app.R;
import com.fin_app.activity.customview.FinTextView;
import com.fin_app.activity.model.banner.BannerResponseModel;
import com.fin_app.activity.network.RetrofitHelper;
import com.fin_app.activity.service.ContactService;
import com.fin_app.activity.utils.Prefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fin_app.activity.utils.Constants.CONTACT_UPLOADED;
import static com.fin_app.activity.utils.Constants.TOKEN;
import static com.fin_app.activity.utils.Constants.TYPE;
import static com.fin_app.activity.utils.Constants.UID;

public class MainActivity extends BaseActivity implements View.OnClickListener{


    private CardView mCardViewApply,mCardViewInvite,
            mCardViewSchedule,mCardViewTrack,
            mCardViewOther,mCardViewContact;

    private AppBarLayout mAppBarlayout;

    private Toolbar toolbar;

    private View progressbar;

    private ImageView imgBanner;

    private TextView txtDay;
    private FinTextView txtLogout;

    private CompositeDisposable mCompositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCompositeDisposable = new CompositeDisposable();

        initView();
        setOnClickListener();
        getService();
        if(!Prefs.getBoolean(MainActivity.this,CONTACT_UPLOADED,false)) {
            Intent serviceIntent = new Intent(MainActivity.this, ContactService.class);
            startService(serviceIntent);
        }
    }

    private void getService() {
        progressbar.setVisibility(View.VISIBLE);
        mCompositeDisposable.add(RetrofitHelper.getInstance()
                .getRequestInstance()
                .getBanner(Prefs.getString(MainActivity.this,UID,"")
                        ,Prefs.getString(MainActivity.this,TOKEN,""))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::handleError));

    }

    private void setOnClickListener() {
        mCardViewApply.setOnClickListener(this);
        mCardViewInvite.setOnClickListener(this);
        mCardViewSchedule.setOnClickListener(this);
        mCardViewTrack.setOnClickListener(this);
        mCardViewOther.setOnClickListener(this);
        mCardViewContact.setOnClickListener(this);
        txtLogout.setOnClickListener(this);

        mAppBarlayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            int scrollRange=-1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                //Check if the view is collapsed
                if (scrollRange + verticalOffset == 0) {
                    toolbar.setBackgroundResource(R.color.colorsAppBackground);
                }else {
                    toolbar.setBackgroundResource(R.color.colorsTransparency);
                }
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        mAppBarlayout.setExpanded(true);
    }

    private void initView() {

        mCardViewApply=(CardView) findViewById(R.id.cardvw_first);
        mCardViewInvite=(CardView) findViewById(R.id.cardvw_second);
        mCardViewSchedule=(CardView) findViewById(R.id.cardvw_third);
        mCardViewTrack=(CardView) findViewById(R.id.cardvw_fourth);
        mCardViewOther=(CardView) findViewById(R.id.cardvw_fifth);
        mCardViewContact=(CardView) findViewById(R.id.cardvw_sixth);
        mAppBarlayout=(AppBarLayout) findViewById(R.id.appbar);
        imgBanner=(ImageView) findViewById(R.id.imgBanner);
        txtDay=(TextView) findViewById(R.id.txtDay);
        txtLogout=(FinTextView) findViewById(R.id.txtLogout);
        progressbar=(View) findViewById(R.id.progressbar);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        Intent intent=null;
        switch (v.getId()){
            case R.id.cardvw_first:
                intent=new Intent(this,ApplyFormActivity.class);
                intent.putExtra(TYPE,"apply");
                break;
            case R.id.cardvw_second:
                intent=new Intent(this,FillFormActivity.class);
                intent.putExtra(TYPE,"invite");
                break;
            case R.id.cardvw_third:
//                intent=new Intent(this,ApplyFormActivity.class);
//                intent.putExtra(TYPE,"schedule");
                Toast.makeText(MainActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                return;

            case R.id.cardvw_fourth:
                intent=new Intent(this,TrackStatusActivity.class);
                intent.putExtra(TYPE,"track");
                break;
            case R.id.cardvw_fifth:
                intent=new Intent(this,ApplyFormActivity.class);
                intent.putExtra(TYPE,"other");
                break;
            case R.id.cardvw_sixth:
//                intent=new Intent(this,FillFormActivity.class);
//                intent.putExtra(TYPE,"contact");
                Toast.makeText(MainActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                return;
            case R.id.txtLogout:
                Prefs.clearData(MainActivity.this);
                intent=new Intent(this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finishAffinity();
                break;

        }
        startActivity(intent);


    }


    private void handleResponse(BannerResponseModel response) {
        progressbar.setVisibility(View.GONE);

        if(response.getSuccess()){
            Glide.with(this)
                    .load(response.getData().getBanner().getImage())
                    .into(imgBanner);

            txtDay.setText(response.getData().getMessage()+"!");

        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void handleError(Throwable error) {
        progressbar.setVisibility(View.GONE);
        Log.d("constants", "handleerror: "+error.getLocalizedMessage());
        Toast.makeText(this, "Error "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }




}
