package com.fin_app.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fin_app.R;
import com.fin_app.activity.adapter.ApplyAdapter;
import com.fin_app.activity.model.apply_other_product.Data;
import com.fin_app.activity.model.banner.BannerResponseModel;
import com.fin_app.activity.network.RetrofitHelper;
import com.fin_app.activity.utils.Prefs;
import com.fin_app.activity.utils.SpacingItemDecoration;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fin_app.activity.utils.Constants.TOKEN;
import static com.fin_app.activity.utils.Constants.TYPE;
import static com.fin_app.activity.utils.Constants.UID;

/**
 * Created by satyendarkumar on 21/10/17.
 */

public class ApplyFormActivity extends BaseActivity implements View.OnClickListener{


    private ImageView mImgBack;
    String type;
    AppBarLayout appBarLayout;
    private RecyclerView mRecyclerView;
    ApplyAdapter adapter;
    TextView txtTitle;
    View progressbar;
    private ImageView imgBanner;
    private TextView txtDay;
    private Toolbar toolbar;
    private CompositeDisposable mCompositeDisposable;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply);
         type=getIntent().getStringExtra(TYPE);
        mCompositeDisposable = new CompositeDisposable();
        initView();

        setOnClickListener();
        getService();
    }

    private void setOnClickListener() {

    }

    private void initView() {


        toolbar=(Toolbar) findViewById(R.id.toolbar);
        appBarLayout=(AppBarLayout) findViewById(R.id.appbar);
        progressbar=(View) findViewById(R.id.progressbar);

        mImgBack=(ImageView) findViewById(R.id.imgBack);
        txtTitle=(TextView) findViewById(R.id.txtTitle);
        mRecyclerView=(RecyclerView) findViewById(R.id.recylerview);
        imgBanner=(ImageView) findViewById(R.id.imgBanner);
        txtDay=(TextView) findViewById(R.id.txtDay);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SpacingItemDecoration(ApplyFormActivity.this, -8));
        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            int scrollRange=-1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                //Check if the view is collapsed
                if (scrollRange + verticalOffset == 0) {
                    toolbar.setBackgroundResource(R.color.colorsAppBackground);
                }else {
                    toolbar.setBackgroundResource(R.color.colorsTransparency);
                }
            }
        });


    }


    private void getService() {
        progressbar.setVisibility(View.VISIBLE);
       if(type.equalsIgnoreCase("apply")) {
           txtTitle.setText(getResources().getString(R.string.str_apply_loan));
           mCompositeDisposable.add(RetrofitHelper.getInstance()
                   .getRequestInstance()
                   .getLoan(Prefs.getString(ApplyFormActivity.this, UID, "")
                           , Prefs.getString(ApplyFormActivity.this, TOKEN, ""))
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribeOn(Schedulers.io())
                   .subscribe(this::handleResponse, this::handleError));
       }else if(type.equalsIgnoreCase("other")){
           txtTitle.setText(getResources().getString(R.string.str_other_product));
           mCompositeDisposable.add(RetrofitHelper.getInstance()
                   .getRequestInstance()
                   .getOther(Prefs.getString(ApplyFormActivity.this, UID, "")
                           , Prefs.getString(ApplyFormActivity.this, TOKEN, ""))
                   .observeOn(AndroidSchedulers.mainThread())
                   .subscribeOn(Schedulers.io())
                   .subscribe(this::handleResponse, this::handleError));
       }

    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent(this,FillFormActivity.class);
        switch (v.getId()) {
        }
    }


    private void handleResponse(ArrayList<Data> response) {
        progressbar.setVisibility(View.GONE);

            adapter=new ApplyAdapter(ApplyFormActivity.this,response);
            mRecyclerView.setAdapter(adapter);
        getBannerService();




    }

    private void handleBannerResponse(BannerResponseModel response) {
        progressbar.setVisibility(View.GONE);

        if(response.getSuccess()){
            Glide.with(this)
                    .load(response.getData().getBanner().getImage())
                    .into(imgBanner);

            txtDay.setText(response.getData().getMessage()+"!");

        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }




    }


    private void handleError(Throwable error) {
        progressbar.setVisibility(View.GONE);
        Log.d("constants", "handleerror: "+error.getLocalizedMessage());
        Toast.makeText(this, "Error "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    private void getBannerService() {
        progressbar.setVisibility(View.GONE);
        mCompositeDisposable.add(RetrofitHelper.getInstance()
                .getRequestInstance()
                .getBanner(Prefs.getString(ApplyFormActivity.this,UID,"")
                        ,Prefs.getString(ApplyFormActivity.this,TOKEN,""))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleBannerResponse,this::handleError));

    }

}
