package com.fin_app.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fin_app.R;
import com.fin_app.activity.model.login.LoginResponseModel;
import com.fin_app.activity.model.otp.OtpResponseModel;
import com.fin_app.activity.network.RetrofitHelper;
import com.fin_app.activity.utils.LogUtils;
import com.fin_app.activity.utils.Prefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fin_app.activity.utils.Constants.DEVICE_ID;
import static com.fin_app.activity.utils.Constants.LOGIN;
import static com.fin_app.activity.utils.Constants.PHONE_NUMBER;
import static com.fin_app.activity.utils.Constants.TOKEN;
import static com.fin_app.activity.utils.Constants.UID;

public class OtpActivity extends BaseActivity {

    private EditText edtOtpNumber;
    private TextView txtTitle,txtVerification,txtChangeMobile,txtResendCode;
    private ImageView imgBack;
    private CompositeDisposable mCompositeDisposable;
    private View progressbar;
    private Toolbar toolBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        mCompositeDisposable = new CompositeDisposable();
        initView();
    }

    private void initView() {
        edtOtpNumber=(EditText) findViewById(R.id.edtOtpNumber);
        txtTitle=(TextView) findViewById(R.id.txtTitle);
        txtVerification=(TextView) findViewById(R.id.txtVerication);
        txtChangeMobile=(TextView) findViewById(R.id.txtChangeMobile);
        txtResendCode=(TextView) findViewById(R.id.txtResendCode);
        imgBack=(ImageView) findViewById(R.id.imgBack);
        toolBar=(Toolbar) findViewById(R.id.toolbar);
        progressbar=(View) findViewById(R.id.progressbar);
        txtTitle.setText("Mobile Verification Code");

        boldText("You would have received an one time verification code on "+Prefs.getString(OtpActivity.this,PHONE_NUMBER,""));
//        txtVerification.setText("You would have received an one time verification code on "+Prefs.getString(OtpActivity.this,PHONE_NUMBER,""));
        toolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txtChangeMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendCode();            }
        });
    }


    public void onClick(View view){

        if(!TextUtils.isEmpty(edtOtpNumber.getText().toString())){
            progressbar.setVisibility(View.VISIBLE);
            mCompositeDisposable.add(RetrofitHelper.getInstance()
                    .getRequestInstance()
                    .getOtp(Prefs.getString(OtpActivity.this,DEVICE_ID,"")
                            ,"android",Prefs.getString(OtpActivity.this,PHONE_NUMBER,"")
                            ,edtOtpNumber.getText().toString())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

        }else {
            Toast.makeText(this, "Please enter the otp number", Toast.LENGTH_SHORT).show();
        }

    }



    private void handleResponse(OtpResponseModel response) {
        progressbar.setVisibility(View.GONE);

        LogUtils.d("jkdsjkfhnbds",response.getMessage());
        if(response.getSuccess()){

            Prefs.saveString(this,TOKEN,response.getData().getToken());
            Prefs.saveString(this,UID,String.valueOf(response.getData().getUid()));
            Prefs.saveBoolean(this,LOGIN,true);
            Intent intent=new Intent(this,MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();


        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void handleError(Throwable error) {
        progressbar.setVisibility(View.GONE);
        Log.d("constants", "handleerror: "+error.getLocalizedMessage());
        Toast.makeText(this, "Error "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    public void boldText(String text){
        final SpannableStringBuilder sb = new SpannableStringBuilder(text);
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
        final StyleSpan iss = new StyleSpan(android.graphics.Typeface.ITALIC); //Span to make text italic
        sb.setSpan(bss, 57, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
//        sb.setSpan(iss, 4, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make last 2 characters Italic
        txtVerification.setText(sb);

          }


          public void resendCode(){
              progressbar.setVisibility(View.VISIBLE);
              mCompositeDisposable.add(RetrofitHelper.getInstance()
                      .getRequestInstance()
                      .login(Prefs.getString(OtpActivity.this,DEVICE_ID,"")
                              ,"android",Prefs.getString(OtpActivity.this,PHONE_NUMBER,""))
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribeOn(Schedulers.io())
                      .subscribe(this::handleResendCode,this::handleResendError));

          }

    private void handleResendCode(LoginResponseModel response) {
        progressbar.setVisibility(View.GONE);
        Log.d("constants", "handleResponse: "+response.getMessage());
        if(response.getSuccess()){

            Toast.makeText(this, "Otp has been in your mobile number", Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void handleResendError(Throwable error) {
        progressbar.setVisibility(View.GONE);
        Log.d("constants", "handleerror: "+error.getLocalizedMessage());
        Toast.makeText(this, "Error "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
