package com.fin_app.activity.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fin_app.R;

/**
 * Created by satyendarkumar on 22/10/17.
 */

public class TrackStatusDetailActivity extends BaseActivity {

    private TextView txtTitle;
    private ImageView imgBack;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trackstatus_detail);
        initView();

    }

    private void initView() {
        txtTitle=(TextView) findViewById(R.id.txtTitle);
        imgBack=(ImageView) findViewById(R.id.imgBack);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        txtTitle.setText(getResources().getString(R.string.str_track_status));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
