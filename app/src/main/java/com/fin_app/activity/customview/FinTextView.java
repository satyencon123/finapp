package com.fin_app.activity.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.fin_app.activity.utils.FontCache;
import com.fin_app.activity.utils.LogUtils;

public class FinTextView extends android.support.v7.widget.AppCompatTextView {

    public FinTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public FinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public FinTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        try {
            LogUtils.d("kfdjkfdjfdjsd","font**********");
            Typeface customFont = FontCache.getTypeface("fontawesome-webfont.ttf", context);
            setTypeface(customFont);
        }catch (Exception e){
            LogUtils.d("kfdjkfdjfdjsd",""+e);
        }
    }
}