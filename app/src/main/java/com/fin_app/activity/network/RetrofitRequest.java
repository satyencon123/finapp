package com.fin_app.activity.network;

import com.fin_app.activity.model.apply_other_product.Data;
import com.fin_app.activity.model.banner.BannerResponseModel;
import com.fin_app.activity.model.login.LoginResponseModel;
import com.fin_app.activity.model.otp.OtpResponseModel;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by satyendarkumar on 23/10/17.
 */

public interface RetrofitRequest {


//    @GET("android/jsonarray/")
//    Observable<List<Android>> register();

    @POST("login")
    @FormUrlEncoded
    Observable<LoginResponseModel> login(@Field("device_id") String device_id,
                                               @Field("device_type") String device_type,
                                               @Field("mobile") String mobile);

    @POST("verify_login")
    @FormUrlEncoded
    Observable<OtpResponseModel> getOtp(@Field("device_id") String device_id,
                                               @Field("device_type") String device_type,
                                               @Field("mobile") String mobile,
                                               @Field("otp") String otp);

    @POST("get_banner")
    @FormUrlEncoded
    Observable<BannerResponseModel> getBanner(@Field("uid") String uid,
                                              @Field("token") String token);

    @POST("get_banner")
    @FormUrlEncoded
    Observable<LoginResponseModel> inviteToMeet(@Field("uid") String uid,
                                              @Field("token") String token,
                                              @Field("name") String name,
                                              @Field("mobile") String mobile,
                                              @Field("address") String address,
                                              @Field("date") String date,
                                              @Field("time") String time);

    @POST("get_products/loan")
    @FormUrlEncoded
    Observable<ArrayList<Data>> getLoan(@Field("uid") String uid,
                                        @Field("token") String token);

    @POST("get_products/other")
    @FormUrlEncoded
    Observable<ArrayList<Data>> getOther(@Field("uid") String uid,
                                        @Field("token") String token);

    @POST("feed_contacts")
    @FormUrlEncoded
    Observable<LoginResponseModel> getContact(@Field("uid") String uid,
                                              @Field("token") String token,
                                              @Field ("contacts") String contacts);
}
