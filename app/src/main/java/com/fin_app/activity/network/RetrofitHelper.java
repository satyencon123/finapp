package com.fin_app.activity.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by satyendarkumar on 23/10/17.
 */

public class RetrofitHelper {
//    public static final String BASE_URL = "https://api.learn2crack.com/";
    public static final String BASE_URL = "http://13.59.26.5/finapp/public/api/";
    public static RetrofitHelper retrofitHelper=new RetrofitHelper();

    private RetrofitHelper(){

    }

    public static RetrofitHelper getInstance(){
        return retrofitHelper;
    }

    public  RetrofitRequest getRequestInstance(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …

// add logging as last interceptor
        httpClient.addInterceptor(logging);

        RetrofitRequest requestInterface = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build().create(RetrofitRequest.class);

        return requestInterface;

    }

}
