package com.fin_app.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fin_app.R;
import com.fin_app.activity.activity.TrackStatusDetailActivity;
import com.fin_app.activity.model.TrackStatusModel;

import java.util.ArrayList;

/**
 * Created by satyendarkumar on 22/10/17.
 */

public class TrackStatusAdapter extends RecyclerView.Adapter<TrackStatusAdapter.TrackStatusViewHolder> {

    ArrayList<TrackStatusModel> alistTrack;
    Context context;
    public TrackStatusAdapter(Context context,ArrayList<TrackStatusModel> alistTrack) {
       this.context=context;
        this.alistTrack=alistTrack;
    }

    @Override
    public TrackStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_trackstatus,null);


        return new TrackStatusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackStatusViewHolder holder, int position) {

        TrackStatusModel trackStatusModel=alistTrack.get(position);


    }

    @Override
    public int getItemCount() {
        return alistTrack.size();
    }

    public class TrackStatusViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLoan,txtStatus,txtDate,txtPrice;

        public TrackStatusViewHolder(View itemView) {
            super(itemView);
            txtLoan=(TextView) itemView.findViewById(R.id.txtLoan);
            txtStatus=(TextView) itemView.findViewById(R.id.txtStatus);
            txtDate=(TextView) itemView.findViewById(R.id.txtDate);
            txtPrice=(TextView) itemView.findViewById(R.id.txtPrice);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, TrackStatusDetailActivity.class);
                    context.startActivity(intent);

                }
            });
        }
    }
}
