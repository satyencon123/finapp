package com.fin_app.activity.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fin_app.R;
import com.fin_app.activity.model.apply_other_product.Data;

import java.util.ArrayList;

/**
 * Created by satyendarkumar on 22/10/17.
 */

public class ApplyAdapter extends RecyclerView.Adapter<ApplyAdapter.TrackStatusViewHolder> {

    ArrayList<Data> alistData;
    Context context;
    public ApplyAdapter(Context context, ArrayList<Data> alistData) {
       this.context=context;
        this.alistData=alistData;
    }

    @Override
    public TrackStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_apply_other_product,null);


        return new TrackStatusViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackStatusViewHolder holder, int position) {

        Data model=alistData.get(position);

      holder.txtLoan.setText(model.getTitle());
        holder.cardvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Under Development", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return alistData.size();
    }

    public class TrackStatusViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLoan,txtStatus,txtDate,txtPrice;
        private CardView cardvw;

        public TrackStatusViewHolder(View itemView) {
            super(itemView);
            txtLoan=(TextView) itemView.findViewById(R.id.txtLoan);
            cardvw=(CardView) itemView.findViewById(R.id.cardvw_first);

        }
    }
}
