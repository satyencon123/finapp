package com.fin_app.activity.application;

import android.app.Application;

import com.fin_app.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by satyendarkumar on 21/10/17.
 */

@ReportsCrashes(
        formKey = "",
        formUri = "",
        mailTo = "satyencon@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)
public class FinApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/OpenSans-Light.ttf")
//                .setFontAttrId(R.attr.fontPath)
                .build()
        );


    }
}
