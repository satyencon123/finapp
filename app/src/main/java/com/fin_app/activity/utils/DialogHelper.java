package com.fin_app.activity.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by satyendarkumar on 28/10/17.
 */

public class DialogHelper {

    public static DialogHelper dialogHelper=new DialogHelper();
    private Context context;
    ProgressDialog progressDialog;


    private DialogHelper(){

    }

    public static DialogHelper getInstance(){
        return dialogHelper;
    }


    public  void dialogShow(Context context,String message)
    {
        this.context=context;
        progressDialog=new ProgressDialog(context);
        progressDialog.setMessage(message+"...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgress(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

    }

    public void dialogDismiss()
    {
        if(progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }

    }

}
