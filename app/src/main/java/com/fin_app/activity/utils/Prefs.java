package com.fin_app.activity.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Handle the Shared preference of the application.
 *
 * @author umesh
 * @Description  this class is used to set preferences value in the ONOTE_PREF file  just like user name ,password phonenumber etc.
 *
 */
public class Prefs {
    private static final Prefs instance = new Prefs();
    private static final String PREF_NAME = "REGO_PREF";

    public static Prefs getInstance() {
        return instance;
    }

    public static void resetPreference(Context context) {
        SharedPreferences info = context.getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = info.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * Save the string into the shared preference.
     *
     * @param context
     *            Context object.
     * @param key
     *            Key to save.
     * @param value
     *            String value associated with the key.
     */
    public static void saveString(Context context, String key, String value) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = info.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Get the string value of key from shared preference.
     *
     * @param key
     *            Key whose value need to be searched.
     * @param defValue
     *            Default value to return in case no such key exist.
     * @return Value associated with the key.
     */
    public static String getString(Context context, String key, String defValue) {
        SharedPreferences info = context.getApplicationContext()
                .getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return info.getString(key, defValue);
    }

    /**
     * Save the boolean into the shared preference.
     *
     * @param context
     *            Context object.
     * @param key
     *            Key to save.
     * @param value
     *            String value associated with the key.
     */
    public static void saveBoolean(Context context, String key, boolean value) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = info.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Get the boolean value of key from shared preference.
     *
     * @param key
     *            Key whose value need to be searched.
     * @param defValue
     *            Default value to return in case no such key exist.
     * @return Value associated with the key.
     */
    public static boolean getBoolean(Context context, String key,
                                     boolean defValue) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return info.getBoolean(key, defValue);
    }

    /**
     * Save the Integer into the shared preference.
     *
     * @param context
     *            Context object.
     * @param key
     *            Key to save.
     * @param value
     *            Integer value associated with the key.
     */
    public static void saveInt(Context context, String key, int value) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = info.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * Get the Integer value of key from shared preference.
     *
     * @param key
     *            Key whose value need to be searched.
     * @param defValue
     *            Default value to return in case no such key exist.
     * @return Value associated with the key.
     */
    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        return info.getInt(key, defValue);
    }




    public static void saveLong(Context context, String key, long value) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = info.edit();
        editor.putLong(key, value);
        editor.commit();
    }


    public static long getLong(Context context, String key, long defValue) {
        SharedPreferences info = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return info.getLong(key, defValue);
    }



    public static void savePreference(SharedPreferences prefs, String key,
                                      boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void setRegistrationPrefs(SharedPreferences prefs,
                                            boolean value) {
        Prefs.savePreference(prefs, "REGISTRATION_CHECK", value);
    }

    public static boolean getRegistrationPrefs(SharedPreferences prefs) {
        return prefs.getBoolean("REGISTRATION_CHECK", false);
    }

    public static void clearData(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    public void saveAccessToken(String accessToken) {
    }

    public void saveFacebookUserInfo(String first_name, String last_name, String email, String gender, String string) {
    }

    public void clearToken() {
    }
}
