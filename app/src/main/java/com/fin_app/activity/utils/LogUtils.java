package com.fin_app.activity.utils;

import android.util.Log;

/**
 * Created by satyendarkumar on 28/10/17.
 */

public class LogUtils {

    public static void d(String type,String message){
        Log.d(type,message);
    }

    public static void v(String type,String message){
        Log.v(type,message);
    }

    public static void e(String type,String message){
        Log.e(type,message);
    }
    public static void w(String type,String message){
        Log.w(type,message);
    }

}
