package com.fin_app.activity.utils;

/**
 * Created by satyendarkumar on 21/10/17.
 */

public class Constants {
    public static String TYPE="type";
    public static String FILL_FORM_TITLE="FILL_FORM_TITLE";
    public static String DEVICE_ID="DEVICE_ID";
    public static String TOKEN="TOKEN";
    public static String UID="UID";
    public static String PHONE_NUMBER="PHONE_NUMBER";
    public static String LOGIN="LOGIN";
    public static String CONTACT_UPLOADED="CONTACT_UPLOADED";

}
