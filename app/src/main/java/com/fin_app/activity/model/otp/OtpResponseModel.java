
package com.fin_app.activity.model.otp;

public class OtpResponseModel {

  
    private Boolean success;

    private String message;

    private Data data;
  
    public Boolean getSuccess() {
        return success;
    }

  
    public void setSuccess(Boolean success) {
        this.success = success;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public Data getData() {
        return data;
    }


    public void setData(Data data) {
        this.data = data;
    }



}
