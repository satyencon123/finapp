package com.fin_app.activity.model;

import java.io.Serializable;

/**
 * Created by satyendarkumar on 22/10/17.
 */

public class TrackStatusModel implements Serializable {


    private String loan_type;
    private String status;
    private String date;
    private String price;

    public String getLoan_type() {
        return loan_type;
    }

    public void setLoan_type(String loan_type) {
        this.loan_type = loan_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
