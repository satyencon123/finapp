
package com.fin_app.activity.model.banner;

public class Data {


    private Banner banner;

    private String message;

    public Banner getBanner() {
        return banner;
    }


    public void setBanner(Banner banner) {
        this.banner = banner;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }



}
