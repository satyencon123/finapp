
package com.fin_app.activity.model.otp;

public class Data {

   
    private String token;

    private Integer uid;

   
    public String getToken() {
        return token;
    }

   
    public void setToken(String token) {
        this.token = token;
    }


    public Integer getUid() {
        return uid;
    }


    public void setUid(Integer uid) {
        this.uid = uid;
    }



}
