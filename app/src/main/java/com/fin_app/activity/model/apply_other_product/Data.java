
package com.fin_app.activity.model.apply_other_product;

public class Data {


    private Integer productTypeId;

    private String title;

    private String slug;



    public Integer getProductTypeId() {
        return productTypeId;
    }


    public void setProductTypeId(Integer productTypeId) {
        this.productTypeId = productTypeId;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getSlug() {
        return slug;
    }


    public void setSlug(String slug) {
        this.slug = slug;
    }


}
