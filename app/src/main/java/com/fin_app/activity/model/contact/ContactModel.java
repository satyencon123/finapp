
package com.fin_app.activity.model.contact;


import java.util.ArrayList;

public class ContactModel {


    private String name;

    private String contact;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getContact() {
        return contact;
    }


    public void setContact(String contact) {
        this.contact = contact;
    }

    public ArrayList<ContactModel> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<ContactModel> contactList) {
        this.contactList = contactList;
    }

    private ArrayList<ContactModel> contactList;


    @Override
    public String toString() {
        String output = "Name: " + getName() + " contact: " + getContact() ;
        return output;

    }
}
